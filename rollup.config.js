import resolve from '@rollup/plugin-node-resolve';
import json from '@rollup/plugin-json';

export default {
    input: 'src/main.js',
    output: {
        file: 'public/main.js',
        format: 'es',
        compact: true
    },
    plugins: [resolve({
        browser: true
    }), json()]
};
